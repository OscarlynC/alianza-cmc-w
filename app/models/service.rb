# Service Model
class Service < ActiveRecord::Base
  include ElasticSearchable
  include ActivityHistory

  def self.query(query)
    { query: { multi_match: {
      query: query,
      fields: [:tittle, :body],
      operator: :and,
      lenient: true }
    }, sort: { id: 'desc' }, size: count }
  end

  # Build index elasticsearch
  def as_indexed_json(_options = {})
    as_json(
      only: [:id, :tittle, :body]
    )
  end
end
